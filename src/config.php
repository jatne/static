<?php
// Website info
define( 'WEBSITE_TITLE', 'Website name' );

define( 'DIR_TEMPALTE', './');

// Assets
define( 'DIR_ASSETS', './assets/');
define( 'DIR_IMG', DIR_ASSETS . 'img/' );
define( 'DIR_CSS', DIR_ASSETS . 'css/');
define( 'DIR_STYLE', DIR_CSS . 'style.css');
define( 'DIR_STYLE_MIN', DIR_CSS . 'style.min.css');
define( 'DIR_JS', DIR_ASSETS . 'js/' );
define( 'DIR_JS_CUSTOM', DIR_JS . 'custom.js' );
define( 'DIR_JS_VENDORS', DIR_JS . 'vendors.js' );
define( 'DIR_JS_CUSTOM_MIN', DIR_JS . 'custom.min.js' );
define( 'DIR_JS_VENDORS_MIN', DIR_JS . 'vendors.min.js' );
define( 'DIR_FONTS', DIR_ASSETS . 'fonts/' );

// Parts
define( 'DIR_PARTS', 'parts/');


function get_part( $part = NULL ) {
    if ( $part && file_exists(DIR_PARTS . $part . '.php') ) {
        return include DIR_PARTS . $part . '.php';
    } else {
        return false;
    }    
}

function get_module( $module = NULL ) {
    if ( $module && file_exists(DIR_PARTS . 'module-' . $module . '.php' ) ) {
        return include DIR_PARTS . 'module-' . $module . '.php';
    } else {
        return false;
    }
}

function get_head() {
   return include_once DIR_PARTS . 'head.php'; 
}

function get_header($header = NULL) {
    if ( is_null($header) ) {
        return include_once DIR_PARTS . 'header.php';
    } else {
        return include_once DIR_PARTS . 'header-' . $header . '.php';
    }
}

function get_footer($footer = NULL) {
    if ( is_null($footer) ) {
        return include_once DIR_PARTS . 'footer.php';
    } else {
        return include_once DIR_PARTS . 'footer-' . $footer . '.php';
    }
}

function get_scripts() {
    $scripts = array(
        "https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js",
        DIR_JS_VENDORS_MIN,
        DIR_JS_CUSTOM,
    );

    $html = NULL;

    foreach ( $scripts as $script ) {
        $html .= '<script src="' . $script . '"></script>';
    }

    return $html;
}

function get_styles() {
    $styles = array(
        DIR_STYLE,
    );

    $html = NULL;

    foreach ( $styles as $style ) {
        $html .= '<link rel="stylesheet" href="' . $style . '">';
    }

    return $html;
}

function get_path_img() {
    return DIR_IMG;
}

function get_website_title() {
    return WEBSITE_TITLE;
}