<?php
require_once( '..\config.php' );

$allPages = glob( DIR_TEMPALTE . '*.php' );

echo '<ul>';
foreach ( $allPages as $page ) {
    // $page = substr($page, 0, -3);
    // $page .= 'html';
    echo '<li><a href="' . $page . '" target="_blank" title="' . $page . '">' . $page . '</a></li>';
}
echo '</ul>';