WebFontConfig = {
	classes: false,
	google : { families: [ 'Roboto:400,500:latin,latin-ext' ] },
	active : function() { sessionStorage.fonts = true; }
};

var wf   = document.createElement('script');
wf.src   = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
wf.type  = 'text/javascript';
wf.async = 'true';
var s    = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(wf, s);

;(function($) {

	$(document).ready(function() {

    });

	$(window).load(function() {

    });

})(jQuery);