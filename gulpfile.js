//PROJECT
var projectUrl = 'static.dev/';

//SOURCES
var dirSrc               = "./src/",
    dirTemplate          = dirSrc + "template/",   
    dirSrcStyleFile      = dirTemplate + "assets/css/style.scss",
    dirSrcStyle          = dirTemplate + "assets/css/",
    dirSrcJs             = dirTemplate + "assets/js/",
    dirSrcJsVendors      = dirSrcJs + "vendors/",
    dirSrcJsVendorsFiles = dirSrcJsVendors + "*.js",
    dirSrcJsCustom       = dirSrcJs + "custom/",
    dirSrcJsCustomFiles  = dirSrcJsCustom + "*.js",
    watchStyleFiles      = dirSrcStyle + "**/*.scss",
    watchJsVendorsFiles  = dirSrcJsVendorsFiles,
    watchJsCustomFiles   = dirSrcJsCustomFiles,
    watchPhpFiles        = dirTemplate + "**/*.php";

//DISTRIBUTION
var dirDist              = "./dist/",
    dirDistStyle         = dirDist + "assets/css/",
    dirDistJsVendors     = dirDist + "assets/js/",
    dirDistJsCustom      = dirDistJsVendors;

var autoprefixerOptions = {
  browsers: [
    'last 2 versions',
    '> 1%',
    'ie >= 9',
    'ff > 40',
    'ie_mob >= 10',
    'android >= 4'
  ]
};

var filesToDist = [
    dirTemplate + '**/*.css',
    dirTemplate + '**/*.js',
    dirTemplate + '**/*.ttf',
    dirTemplate + '**/*.otf',
    dirTemplate + '**/*.eot',
    dirTemplate + '**/*.woff',
    dirTemplate + '**/*.woff2',
    dirTemplate + '**/*.mov',
    dirTemplate + '**/*.mp4',
    dirTemplate + '**/*.webm',
    dirTemplate + '**/*.svg',
    dirTemplate + '**/*.ico',
    dirTemplate + '**/*.jpg',
    dirTemplate + '**/*.png',
    dirTemplate + '**/*.gif',
    '!node_modules/**/*',
    '!style.css.map',
    '!gulpfile.js',
    '!.gitignore',
    '!.git',
];

//GULP PLUGINS
var gulp         = require("gulp"),
    php2html     = require("gulp-php2html"),
    sass         = require('gulp-sass'),
    minifycss    = require('gulp-uglifycss'),
    autoprefixer = require('gulp-autoprefixer'),
    concat       = require('gulp-concat'),
    uglify       = require('gulp-uglify'),
    rename       = require('gulp-rename'),
    lineec       = require('gulp-line-ending-corrector'),
    filter       = require('gulp-filter'),
    sourcemaps   = require('gulp-sourcemaps'),
    sort         = require('gulp-sort'),
    browserSync  = require('browser-sync').create(),
    reload       = browserSync.reload;    
    
//TASKS
gulp.task('browser-sync', function() {
   return browserSync.init({
        proxy        : projectUrl,
        open         : false,
        injectChanges: true,
        online       : true,
        // port         : 7000
    });
});

gulp.task('style', function() {
    return gulp.src( dirSrcStyleFile )
        .pipe( sourcemaps.init() )
        .pipe( sass( {
            errLogToConsole: true,
            outputStyle    : 'expanded',
            // outputStyle    : 'compressed',
            // outputStyle    : 'nested',
            // outputStyle    : 'compact',
            precision      : 10
        } ) )
        .on('error', console.log.bind(console))
        .pipe( sourcemaps.write( { includeContent: false } ) )
        .pipe( sourcemaps.init( { loadMaps: true } ) )
        .pipe( autoprefixer( autoprefixerOptions ) )
        .pipe( sourcemaps.write( dirSrcStyle ) )
        .pipe( lineec() )
        .pipe( gulp.dest( dirSrcStyle ) )
        .pipe( filter( '**/*.css' ) )
        .pipe( browserSync.stream() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( minifycss( { maxLineLen: 0 } ) )
        .pipe( lineec() )
        .pipe( gulp.dest( dirSrcStyle ) )
        .pipe( filter( '**/*.css' ) )
        .pipe( browserSync.stream() );
});

gulp.task('vendors', function() {
    return gulp.src( dirSrcJsVendorsFiles )
        .pipe( concat( 'vendors.js' ) )
        .pipe( lineec() )
        .pipe( gulp.dest( dirSrcJs ) )
        .pipe( rename( {
            basename: 'vendors',
            suffix: '.min'
        } ) )
        .pipe( uglify() )
        .pipe( lineec() )
        .pipe( gulp.dest( dirSrcJs ) );
});

gulp.task('custom', function() {
    return gulp.src( dirSrcJsCustomFiles )
        .pipe( concat( 'custom.js' ) )
        .pipe( lineec() )
        .pipe( gulp.dest( dirSrcJs ) )
        .pipe( rename( {
            basename: 'custom',
            suffix: '.min'
        } ) )
        .pipe( uglify() )
        .pipe( lineec() )
        .pipe( gulp.dest( dirSrcJs ) );
});

gulp.task('build', ['style', 'vendors', 'custom'], function() {
    return console.log('Zadanie "build" zakończone');
});

gulp.task('watch', ['style', 'vendors', 'custom', 'browser-sync'], function() {
	gulp.watch( watchPhpFiles, reload );
	gulp.watch( watchStyleFiles, [ 'style' ] );
	gulp.watch( watchJsVendorsFiles, [ 'vendors', reload ] );
	gulp.watch( watchJsCustomFiles, [ 'custom', reload ] );
});

gulp.task('php2html', function() {
    return gulp.src( dirTemplate + "*.php" )
        .pipe(php2html())
        .pipe(gulp.dest( dirDist ));
});

gulp.task('dist', ['build', 'php2html'], function() {
    return gulp.src(filesToDist).pipe(gulp.dest( dirDist ));
});

gulp.task( 'default', ['watch'], function() {
    return console.log('build+watch');
});

